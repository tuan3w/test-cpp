#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <string>

template <class T>
class Shape {
    protected:
        std::string name;
    public:
        Shape(std::string name);
        std::string get_name();
};

#endif