#ifndef REC_H
#define REC_H
#include "shape.hpp"

template <class T>
class Rec : public Shape<T> {
   public:
       Rec(std::string name): Shape<T>(name){
       }
};
#endif
