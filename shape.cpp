#include "shape.hpp"

template <class T>
Shape<T>::Shape(std::string name) {
    this->name = name;
}


template <class T>
std::string Shape<T>::get_name() {
    return this->name;
}
