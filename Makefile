all: app

app: main.o rec.o shape.o
	g++ -o app main.o rec.o shape.o

main.o: main.cpp 
	g++ -c main.cpp

rec.o: rec.cpp rec.hpp
	g++ -c rec.cpp

shape.o: shape.cpp shape.hpp
	g++ -c shape.cpp

clean:
	rm -rf *.o